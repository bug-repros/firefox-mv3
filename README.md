This reproduces a bug in Firefox's Manifest V3 implementation.

It includes a content script that should add a red border to websites.
You can run it using:

```sh
git clone git@gitlab.com:bug-repros/firefox-mv3.git
cd firefox-mv3
npm install
npm run run
```

In the newly-opened Firefox window, go into `about:addons` and allow "Borderify"
to "Access your data for all websites". Then visit a website, e.g.
https://mozilla.org.

Due to the bug, you will not see a red border. However, if you revert commit
`bed4713287dacdb8c4eb8e106728926e02c9a24f`, the above steps _will_ result in a
red border. Also, if you change the version in `manifest.json` to 2, you will
also be able to see the red border.

```sh
git revert bed4713287dacdb8c4eb8e106728926e02c9a24f
```
